/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * js-sort
 * @see {@link https://framagit.org/tla/js-sort}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/js-sort/blob/master/LICENSE}
 *
 * @method sort.asc
 * @argument {Array} object - [ 1 , 2 , 3 ] || [ { n : 1 } , { n : 2 } , { n : 3 } ]
 * @argument {string} [objectField] - object field if array is an object array
 * @argument {string} [lang = window.navigator.language] - language
 *
 * @method sort.desc
 * @argument {Array} object - [ 1 , 2 , 3 ] || [ { n : 1 } , { n : 2 } , { n : 3 } ]
 * @argument {string} [objectField] - object field if array is an object array
 * @argument {string} [lang = window.navigator.language] - language
 * */
var sort = ( function () {
    
    var obj;
    
    if ( !String.prototype.trim ) {
        Object.defineProperty( String.prototype , "trim" , {
            enumerable : false , writable : false ,
            value : function () {
                return this.toString().replace( /^[\s\u00A0]+|[\s\u00A0]+$/g , '' );
            }
        } );
    }
    
    var localCompare = ( function () {
        
        var bcpSupport , intlSupport , intlCache = {};
        
        if ( ''.localeCompare ) {
            
            intlSupport = !!window.Intl && !!window.Intl.Collator;
            
            bcpSupport = ( function () {
                try {
                    "a".localeCompare( "b" , "i" );
                } catch ( e ) {
                    return ( e.name + '' ) == "RangeError";
                }
                return false;
            } )();
            
            // std localeCompare
            if ( !bcpSupport ) {
                return function ( a , b ) {
                    return a.localeCompare( b );
                };
            }
            
            // adv localeCompare
            if ( !intlSupport ) {
                return function ( a , b , c ) {
                    return a.localeCompare( b , c );
                };
            }
            
            // Intl.Collator
            return function ( a , b , c ) {
                
                // default language
                if ( !c && !( c = window.navigator.language || 'en' ) ) {
                    !intlCache[ ( c = 'default' , c ) ] && ( intlCache[ c ] = new Intl.Collator() );
                }
                
                // custom language
                else if ( !intlCache[ c ] ) {
                    intlCache[ c ] = new Intl.Collator( c );
                }
                
                return intlCache[ c ].compare( a , b );
                
            };
            
        }
        
        // sad polyfill
        return compare;
        
    } )();
    
    var sort = ( function () {
        
        var utc = /^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}T/g;
        
        function compare ( a , b ) {
            return a > b ? 1 : b > a ? -1 : 0;
        }
        
        function canbefloat ( n ) {
            var a = parseFloat( n );
            return isfloat( a ) && a == n;
        }
        
        function tonumber ( s ) {
            return ( /^[0-9.]+\b/g.exec( s ) || [ '' ] )[ 0 ].trim() || null;
        }
        
        function utc_res_test ( _ ) {
            return ( utc.lastIndex = utc.index = 0, utc ).test( _ );
        }
        
        function norm ( s ) {
            return ( s || '' ).toString().toLowerCase().replace( /[\s\u00A0]+/g , ' ' ).trim();
        }
        
        function isfloat ( n ) {
            return exist( n ) && typeof n == 'number' && isFinite( n );
        }
        
        function compareNUM ( a , b ) {
            if ( canbefloat( ( a = tonumber( a ) , a ) ) && canbefloat( ( b = tonumber( b ) , b ) ) ) {
                return compare( +a , +b );
            }
        }
        
        return function ( a , b , lg ) {
            var c , d , ta , tb;
            
            if ( c = compareNUM( a , b ) ) {
                return c;
            }
            
            if ( utc_res_test( a ) && utc_res_test( b ) ) {
                return compare( Date.parse( a ) , Date.parse( b ) );
            }
            
            a = norm( a );
            b = norm( b );
            
            for ( var i = 0 , l = Math.max( a.length , b.length ) ; i < l ; i++ ) {
                
                if ( ( ta = a[ i ] || '' , ta ) == ( tb = b[ i ] || '' , tb ) ) {
                    continue;
                }
                
                if ( c = compareNUM( a.slice( i ) , b.slice( i ) ) ) {
                    return c;
                }
                
                if ( d = localCompare( ta , tb , lg ) , d !== 0 ) {
                    return d;
                }
                
            }
            
            return compare( a.length , b.length );
        };
        
    } )();
    
    return obj = {
        asc : function ( array , objectField , lang ) {
            if ( objectField ) {
                return array.slice( 0 ).sort( function ( a , b ) {
                    return sort( a[ objectField ] , b[ objectField ] , lang );
                } );
            }
            
            if ( lang ) {
                return array.slice( 0 ).sort( function ( a , b ) {
                    return sort( a , b , lang );
                } );
            }
            
            return array.slice( 0 ).sort( sort );
        } ,
        
        desc : function ( a , b , c ) {
            return obj.asc( a , b , c ).reverse();
        }
    };
    
} )();